# devops-ansible

1. Создайте плэйбук, выполняющий установку веб-сервера Apache на управляемом хосте со следующими требованиями:
- установка пакета httpd;
- включение службы веб-сервера и проверка, что он запущен;
- создание файла /var/www/html/index.html с текстом “Welcome to my web server”;
- используйте модуль firewalld для того, чтобы открыть необходимые для работы веб-сервера порты брендмауэра.
2. Создайте плэйбук который:
- удалит httpd с управляемых хостов;
- удалит файл /var/www/html/index.html;
- закроет на фаерволе порты, используемые веб-сервером.
3. Создайте плейбук для изменения файла /etc/default/grub. Он должен добавить параметры net.ifnames=0 и biosdevname=0 в строку, выполняющую загрузку ядра. Выполните grub2-mkconfig, чтобы записать изменения в /etc/default/grub. Используйте модуль lineinfile для изменения конфигурационного файла.
4. Напишите плейбук для создания пользователей Alice, Bob, Carol. Для каждого пользователя нужно задать имя, адрес почты в комментарии (username@example.com), домашнюю папку, пароль в зашифрованном виде – в виде зашифрованной переменной или из отдельного шифрованного файла на выбор. Кроме пароля больше ничего шифровать не нужно. У уже созданных аккаунтов пароль менять не нужно.
5. Сделайте роль для установки Apache, используя плейбук из первого домашнего задания. Требования те же:
установка пакета httpd;
- включение службы веб-сервера и проверка, что он запущен;
- создание файла /var/www/html/index.html;
- открытие необходимых для работы веб-сервера портов брендмауэра.
Хотелось бы увидеть использование переменных (в том числе фактов), хендлеров, шаблонов и т.п., что мы рассматривали в рамках нашего курса.
6. Напишите роль, которая устанавливает и включает FTP (пакет vsftpd), открывает необходимые порты. Определите в переменных необходимые параметры конфигурации ftp-сервера и используйте их в шаблоне для файла конфигурации vsftpd.conf:
- разрешен анонимный доступ в папку /var/ftp/pub и аплоад файлов в папку /var/ftp/pub/upload;
- настроены необходимые разрешения и соответствующий SELinux контекст: "ftpd_anon_write" boolean - значение "on" (edited).
